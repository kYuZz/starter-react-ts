import React from 'react'
import type { ReactElement } from 'react'

const App = (): ReactElement => {
  return <div>hello world</div>
}

export default App
